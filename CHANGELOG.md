Changelog: <br>
8.12. - Dinesh A., Fabian G., Emin K. 11:20-13:00 Uhr -- Planung <br>
10.12. - Dinesh A., Fabian G., Emin K. 9:45-12:00 Uhr -- Anfang der Praxis <br>
17.12. - Fabian G., Emin K. 10:00-11:00 Uhr -- Fehlersuche des momentanen Codes <br>
26.12. - Dinesh A., Emin K. 20:00-20.30 Uhr -- Besprechung <br>
27.12. - Dinesh A., Fabian G., Emin K. 20:00-22:40 Uhr -- komplette Überarbeitung <br>
29.12. - Dinesh A., Emin K. 20:00-23:00 Uhr--Spieler 2 in den Code miteinbeziehen<br>
3.1. - Dinesh A., Fabian G., Emin K. 20:00-21:30 -- Fehlerbehebung <br>
4.1. - Dinesh A., Fabian G. 20:00-22:30 Uhr -- Fehlerbehebung <br>
5.1. - Dinesh A., Fabian G., Emin K. - 18:00-21:00 Uhr -- Fertigstellung des Projekts mit 4x4 Feld <br>
6.1. - Dinesh A., Fabian G., Emin K. - 17:00-18:30 Uhr -- Ausweitung auf 6x6 Feld <br>